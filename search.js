// var filename is declared in treemap.js
var search_filename = filename.substring(0, filename.lastIndexOf('.')+1) + "concepts.json";

d3.json(search_filename, function(json) {
  var concept_heat = Object();
  Object.keys(json).map(function(c) {
    concept_heat[json[c].name.toLowerCase()] = json[c];
  });
  var search = document.getElementById('search');
  search.addEventListener('input', heatmap);
  // event 'input' is also dispatched from treemap.js display() transition()

  function heatmap() {
    if(concept_heat[search.value.toLowerCase()] != undefined) {
      Object.keys(concept_heat[search.value.toLowerCase()]).forEach(function(cluster) {
        d3.select('#id' + cluster)
          .style('fill', '#e66')
          .style('fill-opacity', concept_heat[search.value.toLowerCase()][cluster]);
      });
    } else {
      d3.selectAll('.parent')
        .style('fill', '#bbb')
        .style('fill-opacity', 0.5);
      d3.selectAll('.child')
        .style('fill', '#bbb')
        .style('fill-opacity', 0.5);
    }
  }
});