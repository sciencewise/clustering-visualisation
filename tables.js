  function show_concepts_tfidf(top_concepts_tfidf){
    if ( top_concepts_tfidf ){
        var top_concepts_tfidf_ = Object.keys(top_concepts_tfidf).map(function(key){return top_concepts_tfidf[key]});    // ES6
        // var top_concepts_tfidf_ = Object.values(top_concepts_tfidf); // ES7

        var content = d3.select("#concepts");
        content.select("#concept_table").remove();
        var table = content.select("#concept_table");
        if(table[0][0] == null){
            table = content.append("table");
            table.transition()
                .duration(750);
            table.attr("id", "concept_table");
        // table.append("caption").text('Top TFIDF concepts');
        table.selectAll("th")
            .data(['Top TFIDF concepts'])
            .enter()
            .append("th")
            .style("border", "1px solid gray")
            .text(function(d){return d});
//        table.attr("style", "border:0px solid black");
        }
        table.style("opacity", 1);
        var tr = table.selectAll("tr")
            .data(top_concepts_tfidf_, function(d) {return d});

        tr.enter()
            .append("tr")
            .append("td")
            .attr("id", function(d, i) {return "tfidf_" + (i+1).toString()})
            .text(function(d) {return d})
            .style("opacity", 0)
            .transition()
                .duration(750)
            .style("opacity", 1);
        tr.exit()
            .transition()
                .duration(750)
            .style("opacity", 0)
            .remove();
    }
    else{
        var content = d3.select("#concepts");
        content.select("#concept_table").remove();
    };
  }

  function show_concepts_df(top_concepts_df){
  	if ( top_concepts_df ){
        var top_concepts_df_ = Object.keys(top_concepts_df).map(function(key){return top_concepts_df[key]});    // ES6
        // var top_concepts_df_ = Object.values(top_concepts_df); // ES7

        var content = d3.select("#df-concepts");
        content.select("#df-concept_table").remove();
        var table = content.select("#df-concept_table");
        if(table[0][0] == null){
            table = content.append("table");
            table.transition()
                .duration(750);
            table.attr("id", "df-concept_table");
        // table.append("caption").text('Top TFIDF concepts');
        table.selectAll("th")
            .data(['Top DF concepts'])
            .enter()
            .append("th")
            .style("border", "1px solid gray")
            .text(function(d){return d});
//        table.attr("style", "border:0px solid black");
        }
        table.style("opacity", 1);
        var tr = table.selectAll("tr")
            .data(top_concepts_df_, function(d) {return d});

        tr.enter()
            .append("tr")
            .append("td")
            .attr("id", function(d, i) {return "tfidf_" + (i+1).toString()})
            .text(function(d) {return d})
            .style("opacity", 0)
            .transition()
                .duration(750)
            .style("opacity", 1);
        tr.exit()
            .transition()
                .duration(750)
            .style("opacity", 0)
            .remove();
	}
	else{
  		var content = d3.select("#df-concepts");
        content.select("#df-concept_table").remove();
	};
  }




  function show_articles(top_articles){
  	if ( top_articles ){
        // var top_articles_ = Object.keys(top_articles).map(function(k) { return top_articles[k] });
        var top_articles_ = Object.keys(top_articles).map(function(key){return top_articles[key]});    // ES6
        // var top_articles_ = Object.values(top_articles); // ES7

        var footer = d3.select("#articles");
        footer.select("#article_table").remove();
        var table = footer.append("table");
        table.attr("id", "article_table");
        table.append("caption").text('Representative articles');
        table.selectAll("th")
            .data(['arxiv id', 'title', 'primary category'])
            .enter()
            .append("th")
            .style("border", "1px solid gray")
            .style("border-radius", "1px solid gray")
            .style("text-align", "center")
            .text(function(d){return d});
//        table.attr("style", "border:0px solid black");
        var tr = table.selectAll("tr")
            .data(top_articles_)
            .enter()
            .append("tr");
        tr.append("td")
            .attr("id", function(d, i) {return "a_id_" + (i+1).toString()})
            .text(function(d) {return d.arxiv_id});
        tr.append("td")
            .attr("id", function(d, i) {return "t_id_" + (i+1).toString()})
            .text(function(d) {return d.title});
        tr.append("td")
            .attr("id", function(d, i) {return "cat_id_" + (i+1).toString()})
            .attr("class", "primary_category")
            .text(function(d) {return d.category});
        // tr.append("td")
        //     .attr("id", function(d, i) {return "cats_id_" + (i+1).toString()})
        //     .attr("class", "secondary_categories")
        //     .text(function(d) {return (d.categories).join('\n')});
        tr.style("opacity", 0).transition().duration(DURATION).style("opacity", 1);
//        tr.style("background", function(d, i) {return i%2 ? "#fff" : "#eee"});
	} else {
        var footer = d3.select("#articles");
        footer.select("#article_table")
            .style("opacity", 1)
            .transition()
                .duration(DURATION)
            .style("opacity", 0)
        .remove();
    };
  }



